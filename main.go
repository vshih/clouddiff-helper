package main

import (
	"bitbucket.org/vshih/clouddiff-helper/internal/executil"
	"bitbucket.org/vshih/clouddiff-helper/internal/install"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/virtao/GoEndian"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
)

type diffRequest struct {
	Cmd  string
	Left struct {
		Name string
		Text string
	}
	Right struct {
		Name string
		Text string
	}
}

type diffResponse struct {
	Cmd        string
	Args       []string
	ExitStatus int
	Output     string
}

var l *log.Logger

func main() {
	var tmpDir = createTempDir()

	l = createLogger(tmpDir)

	var uninstallFlag bool
	flag.BoolVar(&uninstallFlag, `u`, false, `Uninstall.`)

	// TODO Add web page, support email address, to --help.

	flag.Parse()

	if uninstallFlag {
		install.DoUninstall()
	} else if flag.NArg() > 0 {
		// We've been called by Chrome, where the extension ID is the first parameter.
		doDiff(tmpDir)
	} else {
		install.DoInstall()
	}
}

func doDiff(tmpDir string) {
	l.Println(`START {`)

	var diffDir = createDiffDir(tmpDir)

	textLen := readRequestLen(os.Stdin)
	l.Printf("Request length: %d\n", textLen)

	bytes := make([]byte, textLen)
	readRequest(os.Stdin, bytes)

	// JSON-decode.
	var request diffRequest
	if err := json.Unmarshal(bytes, &request); err != nil {
		l.Println(`Failed to parse request text:`)
		l.Panicln(err)
	}

	l.Printf("cmd: %s\n", request.Cmd)
	l.Printf("left.name: %s\n", request.Left.Name)
	//l.Printf("left.text: %s\n", request.Left.Text)
	l.Printf("right.name: %s\n", request.Right.Name)
	//l.Printf("right.text: %s\n", request.Right.Text)

	// Write files.
	leftName := path.Join(diffDir, request.Left.Name)
	rightName := path.Join(diffDir, request.Right.Name)

	if err := writeFile(leftName, request.Left.Text); err != nil {
		message := fmt.Sprintf(`Failed to create "%s": %s`, leftName, err.Error())
		sendResponse(diffResponse{Output: message})
		l.Panicln(message)
	}
	if err := writeFile(rightName, request.Right.Text); err != nil {
		message := fmt.Sprintf(`Failed to create "%s": %s`, rightName, err.Error())
		sendResponse(diffResponse{Output: message})
		l.Panicln(message)
	}

	// Change directory.
	if err := os.Chdir(diffDir); err != nil {
		message := fmt.Sprintf(`Chdir("%s") failed: %s`, diffDir, err.Error())
		sendResponse(diffResponse{Output: message})
		l.Panicln(message)
	}

	response := spawnDiff(request.Cmd, request.Left.Name, request.Right.Name)

	sendResponse(response)

	if os.Chdir(`..`) != nil {
		if err := os.RemoveAll(diffDir); err != nil {
			l.Print(fmt.Sprintf(`RemoveAll("%s") failed: %s`, tmpDir, err.Error()))
			// Not a biggie; fall through.
		}
	}

	l.Println(`END }`)
}

// Getters/globals.

func getNativeByteOrder() binary.ByteOrder {
	var nativeByteOrder binary.ByteOrder
	nativeByteOrder = binary.LittleEndian
	if endian.IsBigEndian() {
		nativeByteOrder = binary.BigEndian
	}

	return nativeByteOrder
}

var nativeByteOrder = getNativeByteOrder()

func createTempDir() string {
	// Create top-level directory.
	dir := path.Join(os.TempDir(), `CloudDiff Helper`)
	// Fail silenly.
	_ = os.Mkdir(dir, 0770)
	return dir
}

// Create log file.
func createLogger(dir string) *log.Logger {
	logf, err := os.OpenFile(path.Join(dir, `clouddiff-helper.log`), os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	return log.New(logf, ``, log.Flags())
}

func createDiffDir(tmpDir string) string {
	dir, err := ioutil.TempDir(tmpDir, `Helper-`)
	if err != nil {
		message := fmt.Sprintf(`Failed to create temporary directory: %s`, err.Error())
		sendResponse(diffResponse{Output: message})
		panic(message)
	}

	return dir
}

// Read message length.
func readRequestLen(file *os.File) int32 {
	var textLen int32
	if err := binary.Read(file, nativeByteOrder, &textLen); err != nil {
		l.Println(`Failed to parse message size:`)
		l.Panicln(err)
	}

	return textLen
}

// Read the message.
func readRequest(file *os.File, buffer []byte) {
	bufferLen := len(buffer)

	if readCount, err := io.ReadAtLeast(file, buffer, bufferLen); err != nil {
		l.Printf("Failed to read %d bytes (%d actual):\n", bufferLen, readCount)
		l.Panicln(err)
	}
}

func writeFile(filePath, text string) error {
	return ioutil.WriteFile(filePath, []byte(text), 0600)
}

func spawnDiff(cmdTemplate string, leftFile string, rightFile string) diffResponse {
	response := diffResponse{Cmd: cmdTemplate, Args: []string{leftFile, rightFile}}

	// Append parameters.
	if !strings.Contains(cmdTemplate, `$1`) {
		response.Cmd += ` $1 $2`
	}

	// Quote then substitute files.
	response.Cmd = strings.Replace(
		strings.Replace(
			response.Cmd,
			`$1`,
			executil.QuoteForShell(leftFile),
			1),
		`$2`,
		executil.QuoteForShell(rightFile),
		1)

	cmd := executil.Command(response.Cmd)

	output, err := cmd.CombinedOutput()
	response.Output += string(output)
	if err != nil {
		// Get exit status.
		response.Output += err.Error()
		if exiterr, ok := err.(*exec.ExitError); ok {
			response.ExitStatus = exiterr.ExitCode()
		}
	}

	return response
}

func sendResponse(response diffResponse) {
	// JSON-encode.
	bytes, err := json.Marshal(response)
	if err != nil {
		l.Printf(`Failed to send response ("%v")`, response)
		l.Panicln(err)
	}

	responseLen := int32(len(bytes))

	if err := binary.Write(os.Stdout, nativeByteOrder, &responseLen); err != nil {
		message := fmt.Sprintf(`Failed to write response ("%v"): %s`, response, err.Error())
		l.Panicln(message)
	}

	os.Stdout.Write(bytes)
}
