// +build !windows

package executil

import (
	"os/exec"
	"strings"
)

// QuoteForShell quotes arg in a way compatible with the target OS' shell.
func QuoteForShell(arg string) string {
	return `'` + strings.Replace(arg, `'`, `'\''`, -1) + `'`
}

// Command constructs an exec.Cmd object to run cmdLine within the target OS' shell.
func Command(cmdLine string) *exec.Cmd {
	cmd := exec.Command(`sh`, `-c`, cmdLine)

	return cmd
}
