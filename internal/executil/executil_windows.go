package executil

import (
	"fmt"
	"os/exec"
	"strings"
	"syscall"
)

// QuoteForShell quotes arg in a way compatible with the target OS' shell.
func QuoteForShell(arg string) string {
	return `"` + strings.Replace(arg, `"`, `^"`, -1) + `"`
}

// Command constructs an exec.Cmd object to run cmdLine within the target OS' shell.
func Command(cmdLine string) *exec.Cmd {
	cmd := exec.Command(`cmd.exe`, ``)
	// Note that no escaping of double-quotes is done; cmd.exe actually expects this.
	cmd.SysProcAttr = &syscall.SysProcAttr{CmdLine: fmt.Sprintf(`/C "%s"`, cmdLine)}
	return cmd
}
