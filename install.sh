#!/bin/sh


#
# install.sh
#
# Support script for Mac/Linux one-liner installation of CloudDiff-Helper:
#
#	$ curl -L https://bitbucket.org/vshih/dropboxdiff-helper/downloads/install.sh | sh
#


set -x
exe=clouddiff-helper_`uname | tr '[A-Z]' '[a-z]'`_`arch`

curl -LO https://bitbucket.org/vshih/clouddiff-helper/downloads/$exe
chmod +x $exe

./$exe
rm -f $exe

