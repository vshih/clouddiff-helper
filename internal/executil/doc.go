// Package executil provides OS-specific logic to make working with `os.exec` easier.
package executil
