package install

import (
	"golang.org/x/sys/windows/registry"
	"log"
	"os"
)

const (
	cRegistryPath = `Software\Google\Chrome\NativeMessagingHosts\` + cNativeMessagingHostName
)

func DoInstall() {
	log.Print(`Installing...`)

	srcPath := os.Args[0]
	manifestPath := installBase(srcPath)

	createRegistry(cRegistryPath, manifestPath)

	// TODO Create user .lnk and home-page link

	log.Print(`Install complete.`)

	finish(0)
}

func DoUninstall() {
	log.Print(`Uninstalling...`)

	// TODO Delete user .lnk and home-page link

	deleteRegistry(cRegistryPath)

	appDir := computeAppDir()
	deleteAppDir(appDir)

	log.Print(`Uninstall complete.`)

	finish(0)
}

func computeAppDir() string {
	return os.ExpandEnv(`$LOCALAPPDATA\` + cNativeMessagingHostDesc)
}

func computeManifestPath(appDir string) string {
	return appDir + `\manifest.json`
}

func createRegistry(registryPath, manifestPath string) {
	log.Print(`Writing registry entry...`)

	key, _, err := registry.CreateKey(registry.CURRENT_USER, registryPath, registry.SET_VALUE|registry.WRITE)
	if err != nil {
		fatal(err)
	}
	defer key.Close()

	if err = key.SetStringValue(``, manifestPath); err != nil {
		fatal(err)
	}
}

func deleteRegistry(registryPath string) {
	log.Print(`Deleting registry entry...`)

	if err := registry.DeleteKey(registry.CURRENT_USER, registryPath); err != nil {
		log.Print(err)
	}
}
