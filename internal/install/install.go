// Package install implements OS-specific installation steps, per Chrome's native messaging host requirements.
package install

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
)

const (
	// This name can only contain lowercase alphanumeric characters, underscores and dots, per
	// https://developer.chrome.com/extensions/nativeMessaging.
	// Must match NATIVE_MESSAGING_HOST_NAME in bitbucket.org/vshih/clouddiff/chrome-extension/service-worker.js.
	cNativeMessagingHostName = `com.vicshih.clouddiff.helper`

	cNativeMessagingHostDesc = `CloudDiff Helper`

	// Development extension ID; override by setting EXTENSION_ID during installation for alternate installations.
	cExtensionId = `mclhnicmmbipgddnijniccihmbdlhogb`
)

type nativeMessagingHostManifest struct {
	Name           string   `json:"name"`
	Description    string   `json:"description"`
	Path           string   `json:"path"`
	Type           string   `json:"type"`
	AllowedOrigins []string `json:"allowed_origins"`
}

func installBase(exeSrcPath string) string {
	appDir := computeAppDir()

	createAppDir(appDir)

	exeDstPath := copyExe(exeSrcPath, appDir)

	manifestPath := computeManifestPath(appDir)
	createManifest(manifestPath, exeDstPath)

	return manifestPath
}

func createAppDir(appDir string) {
	log.Print(`Creating application directory...`)

	if err := os.Mkdir(appDir, 0755); err != nil {
		if os.IsExist(err) {
			// The directory exists already; treat as non-fatal.
			log.Print(err)
		} else {
			fatal(err)
		}
	}
}

func deleteAppDir(appDir string) {
	log.Print(`Deleting application directory...`)

	if err := os.RemoveAll(appDir); err != nil {
		log.Print(err)
	}
}

func copyExe(exePath, appDir string) string {
	log.Print(`Copying executable...`)

	exeContent, err := ioutil.ReadFile(exePath)
	if err != nil {
		fatal(err)
	}

	dstPath := filepath.Join(appDir, filepath.Base(exePath))
	if err = ioutil.WriteFile(dstPath, exeContent, 0755); err != nil {
		fatal(err)
	}

	return dstPath
}

func createManifest(manifestPath, exeDstPath string) {
	log.Print(`Writing manifest...`)

	effectiveId := os.Getenv(`EXTENSION_ID`)
	if effectiveId != `` {
		log.Printf("Using EXTENSION_ID [%s]\n", effectiveId)
	} else {
		effectiveId = cExtensionId
		log.Printf("Using DEVELOPMENT EXTENSION_ID [%s]\n", effectiveId)
	}

	manifest := nativeMessagingHostManifest{
		Name:           cNativeMessagingHostName,
		Description:    cNativeMessagingHostDesc,
		Path:           exeDstPath,
		Type:           `stdio`,
		AllowedOrigins: []string{`chrome-extension://` + effectiveId + `/`}}

	manifestText, err := json.MarshalIndent(manifest, ``, `  `)
	if err != nil {
		fatal(err)
	}

	// In some cases the containing directory doesn't exist yet.
	if err := os.Mkdir(path.Dir(manifestPath), 0755); err != nil {
		if os.IsExist(err) {
			// The directory exists already; treat as non-fatal.
			log.Print(err)
		} else {
			fatal(err)
		}
	}
	if err := ioutil.WriteFile(manifestPath, manifestText, 0644); err != nil {
		fatal(err)
	}
}

// The deleteManifest function deletes the generated manifest file.
// In the Windows case, it's covered by deleteAppDir, so therefore not used.
func deleteManifest(manifestPath string) {
	log.Print(`Deleting manifest...`)

	if err := os.Remove(manifestPath); err != nil {
		log.Print(err)
	}
}

func fatal(v ...interface{}) {
	log.Print(v)
	finish(2)
}

func fatalf(format string, v ...interface{}) {
	log.Printf(format, v)
	finish(2)
}

func finish(code int) {
	if code != 0 {
		log.Print(`Operation failed.`)
	}
	fmt.Print(`Press ENTER to continue ... `)
	_, _ = bufio.NewReader(os.Stdin).ReadString('\n')
	os.Exit(code)
}
