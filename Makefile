
TARGET_BASE = clouddiff-helper

TARGETS = \
	$(TARGET_BASE)_darwin_amd64 \
	$(TARGET_BASE)_darwin_arm64 \
	$(TARGET_BASE)_linux_amd64 \
	$(TARGET_BASE)_linux_arm64 \
	$(TARGET_BASE)_windows.exe

SOURCES = $(shell /usr/bin/find . -name \*.go)

SHELL = bash

help:
	@echo Valid targets:
	@echo '  all'
	@for t in $(TARGETS); do echo "  $$t"; done

all:	$(TARGETS)


$(TARGET_BASE)_darwin_amd64:	$(SOURCES)
	GOOS=darwin GOARCH=amd64 go build -o $@

$(TARGET_BASE)_darwin_arm64:	$(SOURCES)
	GOOS=darwin GOARCH=arm64 go build -o $@

$(TARGET_BASE)_linux_amd64:	$(SOURCES)
	GOOS=linux GOARCH=amd64 go build -o $@

$(TARGET_BASE)_linux_arm64:	$(SOURCES)
	GOOS=linux GOARCH=arm64 go build -o $@

$(TARGET_BASE)_windows.exe:	$(SOURCES)
	GOOS=windows GOARCH=amd64 go build -o $@

