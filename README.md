
# README

CloudDiff-Helper is the installable helper for the
[CloudDiff Chrome Extension](https://github.com/vshih/CloudDiff)
which triggers any user-installed diff program.  I wrote about it at
[blog.vicshih.com](http://blog.vicshih.com/2011/09/clouddiff-chrome-extension.html).

## To Install

* Windows

    1. Download [clouddiff-helper_windows.exe](https://bitbucket.org/vshih/clouddiff-helper/downloads/clouddiff-helper_windows.exe).
    2. Double-click the executable to install.  To bypass Windows SmartScreen, click the "More Info" text, then the "Run Anyway" button.
    3. The original download can now be deleted.

* Mac/Linux

    1. Open Terminal and run the following:

        $ curl -L https://bitbucket.org/vshih/clouddiff-helper/downloads/install.sh | sh

## To Uninstall

* Windows

    1. In a Command Prompt window, run:

        \> "%LOCALAPPDATA%\CloudDiff Helper\clouddiff-helper_windows.exe" -u

        The program will try to delete the `%LOCALAPPDATA%\CloudDiff Helper` directory, so it is better to run this from outside that directory.

* Mac
    1. In a Terminal window, run:

        $ "~/Library/CloudDiff Helper/clouddiff-helper_darwin_$(arch)" -u

        The program will try to delete the `~/Library/CloudDiff Helper` directory, so it is better to run this from outside that directory.

* Linux
    1. In a shell or terminal window, run:

        $ "~/.com.vicshih.clouddiff.helper/clouddiff-helper_linux_$(arch)" -u

        The program will try to delete the `~/.com.vicshih.clouddiff.helper` directory, so it is better to run this from outside that directory.

## Contact

For issues please go to [https://bitbucket.org/vshih/clouddiff-helper/issues?status=new&status=open](https://bitbucket.org/vshih/clouddiff-helper/issues?status=new&status=open).

