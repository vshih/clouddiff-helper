package install

import (
	"log"
	"os"
)

func DoInstall() {
	log.Print(`Installing...`)

	exePath := os.Args[0]
	_ = installBase(exePath)

	log.Print(`Install complete.`)

	finish(0)
}

func computeAppDir() string {
	return os.ExpandEnv(`$HOME/Library/` + cNativeMessagingHostDesc)
}

// Function computeManifestPath computes the OS- and browser- specific location for the manifest file.
func computeManifestPath(appDir string) string {
	browserDir := `Google/Chrome`
	if os.Getenv(`BROWSER`) == `Chromium` {
		browserDir = `Chromium`
	}

	return os.ExpandEnv(
		`$HOME/Library/Application Support/` + browserDir + `/NativeMessagingHosts/` + cNativeMessagingHostName + `.json`)
}

func DoUninstall() {
	log.Print(`Uninstalling...`)

	appDir := computeAppDir()
	manifestPath := computeManifestPath(appDir)
	deleteManifest(manifestPath)
	deleteAppDir(appDir)

	log.Print(`Uninstall complete.`)

	finish(0)
}
